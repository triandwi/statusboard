function setMinDateFrom() {
    var from = document.getElementById("durationStart");

    var today = new Date(Date.now());

    var month = ("0" + (today.getMonth() + 1)).slice(-2)
    var minFrom = today.getFullYear() + "-" + month + "-" + today.getDate();
    from.setAttribute("min",minFrom);
}

function setMinDateTo(from) {
    var to = document.getElementById("durationEnd");

    var minTo = from.value;
    to.setAttribute("min",minTo);
}