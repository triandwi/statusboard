//create the dropdown with the list of persons for item management
function createDropdownItem(data) {
    //get the parent
    var parent = document.getElementById("addItemPerson");
    //set it to zero (needed for reload)
    parent.innerHTML = "";

    //create and set style
    var dropdown = document.createElement("select");
    dropdown.setAttribute("id", "itemPerson");
    dropdown.classList.add("w3-select");

    //create options
    data.forEach(element => {
        var option = document.createElement("option")
        option.appendChild(document.createTextNode(element.ID));
        option.setAttribute("value", element.ID);
        dropdown.appendChild(option);

    });

    //append to parent
    parent.appendChild(dropdown);

}

//create the dropdown with the list of persons for item management - case vacation
function createDropdownItemVacation(data) {
    //get the parent
    var parent = document.getElementById("addItemInfoVacation");
    //set it to zero (needed for reload)
    parent.innerHTML = "";

    //create and set style
    var dropdown = document.createElement("select");
    dropdown.setAttribute("id", "itemBuddy");
    dropdown.classList.add("w3-select");

    //add "unknown" option
    var optionUnknown = document.createElement("option");
    optionUnknown.appendChild(document.createTextNode("unknown"));
    dropdown.appendChild(optionUnknown);

    //create options
    data.forEach(element => {
        var option = document.createElement("option");
        option.appendChild(document.createTextNode(element.ID));
        option.setAttribute("value", element.ID);
        dropdown.appendChild(option);

    });

    //append to parent
    parent.appendChild(dropdown);

}

//create the dropdown with the list of persons for item management - case sick
function createDropdownItemBuddy(data) {
    //get the parent
    var parent = document.getElementById("addItemInfoBuddy");
    //set it to zero (needed for reload)
    parent.innerHTML = "";

    //create and set style
    var dropdown = document.createElement("select");
    dropdown.setAttribute("id", "itemBuddy");
    dropdown.classList.add("w3-select");

    //add "unknown" option
    var optionUnknown = document.createElement("option");
    optionUnknown.appendChild(document.createTextNode("unknown"));
    dropdown.appendChild(optionUnknown);

    //create options
    data.forEach(element => {
        var option = document.createElement("option")
        option.appendChild(document.createTextNode(element.ID));
        option.setAttribute("value", element.ID);
        dropdown.appendChild(option);

    });

    //append to parent
    parent.appendChild(dropdown);

}

//when unknown was selected and needs to be changed, this is shown in a modal
function createDropdownUnknownChange(data) {
    //get the parent
    var parent = document.getElementById("unknownChange");
    //set it to zero (needed for reload)
    parent.innerHTML = "";

    //create and set style
    var dropdown = document.createElement("select");
    dropdown.setAttribute("id", "unknownDropdown");
    dropdown.classList.add("w3-select");

    //create options
    data.forEach(element => {
        var option = document.createElement("option")
        option.appendChild(document.createTextNode(element.ID));
        option.setAttribute("value", element.ID);
        dropdown.appendChild(option);

    });

    //append to parent
    parent.appendChild(dropdown);

}


//show only the info that is relevant to the selected type
function hideInfo() {
    //get all info elements
    var elementList = document.getElementsByClassName("toggleVisibility");

    //get selected type
    var type = document.getElementById("addItemType").value;

    //loop through and set visibility
    for (var i = 0; i < elementList.length; i++) {
        switch (type) {
            case "vacation":
                if (elementList[i].id == "toggleVacation") {
                    elementList[i].classList.add("visible");
                    elementList[i].classList.remove("hidden");
                }
                else {
                    elementList[i].classList.add("hidden");
                    elementList[i].classList.remove("visible");
                }
                break;
            case "sick":
                if (elementList[i].id == "toggleBuddy") {
                    elementList[i].classList.add("visible");
                    elementList[i].classList.remove("hidden");
                }
                else {
                    elementList[i].classList.add("hidden");
                    elementList[i].classList.remove("visible");
                }
                break;

            case "triage":
                if (elementList[i].id == "toggleTriage") {
                    elementList[i].classList.add("visible");
                    elementList[i].classList.remove("hidden");
                }
                else {
                    elementList[i].classList.add("hidden");
                    elementList[i].classList.remove("visible");
                }
                break;

            default:
                if (elementList[i].id == "toggleText") {
                    elementList[i].classList.add("visible");
                    elementList[i].classList.remove("hidden");
                }
                else {
                    elementList[i].classList.add("hidden");
                    elementList[i].classList.remove("visible");
                }
                break;
        }

    };
}