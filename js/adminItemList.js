//generate the item list
function displayItem(data) {
    var list = document.getElementById("itemList");
    list.innerHTML = ""; //required for reload

    //create header
    var headerDiv = document.createElement("div");
    headerDiv.classList.add("w3-container");
    list.appendChild(headerDiv);

    var emptyHeader = document.createElement("div");
    emptyHeader.classList.add("w3-quarter");
    emptyHeader.appendChild(document.createTextNode("-"));
    emptyHeader.classList.add("w3-padding");
    var personHeader = document.createElement("div");
    personHeader.classList.add("w3-quarter");
    personHeader.classList.add("bold");
    personHeader.appendChild(document.createTextNode("Person"));
    personHeader.classList.add("w3-padding");
    var typeHeader = document.createElement("div");
    typeHeader.classList.add("w3-quarter");
    typeHeader.classList.add("bold");
    typeHeader.appendChild(document.createTextNode("Type"));
    typeHeader.classList.add("w3-padding");
    var infoHeader = document.createElement("div");
    infoHeader.classList.add("w3-quarter");
    infoHeader.classList.add("bold");
    infoHeader.appendChild(document.createTextNode("Additional info"));
    infoHeader.classList.add("w3-padding");

    headerDiv.appendChild(emptyHeader);
    headerDiv.appendChild(personHeader);
    headerDiv.appendChild(typeHeader);
    headerDiv.appendChild(infoHeader);


    data.forEach(element => {
        var row = addItem(element.ID, element.person, element.type, element.info, "saved");
        list.appendChild(row);
    })
}


//create a new row in the item list with the supplied data
function addItem(id, person, type, info, saveStatus) {
    //create one row
    var rowDiv = document.createElement("div");
    rowDiv.setAttribute("id", "item_" + id);
    rowDiv.classList.add("w3-container");
    rowDiv.classList.add("oooItem");
    rowDiv.classList.add("w3-border-grey");
    rowDiv.classList.add("w3-border-top");


    //split row into four and add data to it
    removeDiv = document.createElement("div");
    removeDiv.classList.add("w3-quarter");
    offsetDiv = document.createElement("div"); //to center the button inside its div
    offsetDiv.classList.add("w3-quarter");
    offsetDiv.classList.add("w3-container");
    removeDiv.appendChild(offsetDiv);
    buttonDiv = document.createElement("div");
    buttonDiv.classList.add("w3-button");
    buttonDiv.classList.add("w3-green");
    buttonDiv.classList.add("w3-half");
    buttonDiv.setAttribute("onclick", "deleteSingleItem(this)");
    buttonDiv.appendChild(document.createTextNode("Remove"));
    removeDiv.appendChild(buttonDiv);

    agentDiv = document.createElement("div");
    agentDiv.classList.add("w3-quarter");
    agentDiv.appendChild(document.createTextNode(person));
    agentDiv.classList.add("w3-padding");


    typeDiv = document.createElement("div");
    typeDiv.classList.add("w3-quarter");
    typeDiv.appendChild(document.createTextNode(translateType(type, "toPublic")));
    typeDiv.classList.add("w3-padding");

    infoDiv = document.createElement("div");
    infoDiv.classList.add("w3-quarter");
    infoDiv.classList.add("w3-padding");
    if (info != "") {

        //for vacation, split into three
        if (info.includes("|")) {
            var infoSplit = info.split(" | ");

            firstSpan = document.createElement("span");
            if (infoSplit[0].includes("unknown")) {
                firstSpan.setAttribute("onclick", "openUnknownModal(this)");
                firstSpan.classList.add("changeable");
            }
            
            firstSpan.appendChild(document.createTextNode(infoSplit[0]));

            secondSpan = document.createElement("span");
            secondSpan.appendChild(document.createTextNode(" | " + infoSplit[1]));

            infoDiv.appendChild(firstSpan)
            infoDiv.appendChild(secondSpan);

        }
        //for sick
        else if (info.includes("unknown")) {
            span = document.createElement("span");
            span.setAttribute("onclick", "openUnknownModal(this)");
            span.appendChild(document.createTextNode(info));
            span.classList.add("changeable");

            infoDiv.appendChild(span);
        }
        //all others
        else { infoDiv.appendChild(document.createTextNode(info)); }
    }
    else {
        infoDiv.appendChild(document.createTextNode("-"));
    }

    if (saveStatus != "saved") {
        agentDiv.classList.add("italic");
        typeDiv.classList.add("italic");
        infoDiv.classList.add("italic");
    }

    rowDiv.appendChild(removeDiv);
    rowDiv.appendChild(agentDiv);
    rowDiv.appendChild(typeDiv);
    rowDiv.appendChild(infoDiv);
    return rowDiv;
}


//read user input and add one new item to the item list
function addItemtoList() {
    var id = document.getElementsByClassName("oooItem").length + 1;
    var person = document.getElementById("itemPerson").value;
    var type = document.getElementById("addItemType").value;
    var info;

    switch (type) {
        case "vacation":
            var buddy = document.getElementById("itemBuddy").value;

            var from = document.getElementById("durationStart").value;
            var to = document.getElementById("durationEnd").value;
            info = buddy + " | " + from + " / " + to;
            break;
        case "sick":
            info = document.getElementById("itemBuddy").value;
            break;
        case "triage":
            info = document.getElementById("addItemInfoTriage").value;

            break;
        default:
            info = document.getElementById("addItemInfoText").value;
    }
    //create new row and append to list
    var newRow = addItem(id, person, type, info, "unsaved");

    var ItemList = document.getElementById("itemList");
    ItemList.appendChild(newRow);

}

//remove one row from the item list
function deleteSingleItem(objectToDelete) {
    var row = objectToDelete.parentNode.parentNode;
    var itemList = row.parentNode;

    itemList.removeChild(row);
}


//put item list into DB
function manageItem() {

    //create new formdata with the fetched data, as it is needed to read out the values on php side then
    var data = createJsonData();
    var dataContainer = new FormData();
    dataContainer.append("data", data);


    var xhp = new XMLHttpRequest();
    xhp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //callback function because it is asynchronous
            load('oooItem', displayItem);
        }
    }
    xhp.open("POST", "php/manageItem.php");
    xhp.send(dataContainer);
}

//because type in DB is not same as visible to end user
function translateType(type, mode) {
    var translatedType;

    if (mode == "toPublic") {
        switch (type) {
            case "vacation":
                translatedType = "Vacation";
                break;
            case "sick":
                translatedType = "Sick leave";
                break;
            case "home":
                translatedType = "Home office";
                break;
            case "other":
                translatedType = "Other";
                break;
            case "triage":
                translatedType = "Triage";
                break;
            case "long":
                translatedType = "Long term";
                break;
        }
    }
    else {
        switch (type) {
            case "Vacation":
                translatedType = "vacation";
                break;
            case "Sick leave":
                translatedType = "sick";
                break;
            case "Home office":
                translatedType = "home";
                break;
            case "Other":
                translatedType = "other";
                break;
            case "Triage":
                translatedType = "triage";
                break;
            case "Long term":
                translatedType = "long";
                break;
        }
    }
    return translatedType;
}


//collect all data from item list and provide it as string (not directly as a json)
function createJsonData() {
    var items = document.getElementsByClassName("oooItem");

    var data = [];

    for (var i = 0; i < items.length; i++) {
        var itemDivs = items[i].childNodes;
        var object = {
            person: itemDivs[1].innerText,
            type: translateType(itemDivs[2].innerText, "toAdmin"),
            info: itemDivs[3].innerText
        }
        data.push(object);
    }


    return JSON.stringify(data);
}


