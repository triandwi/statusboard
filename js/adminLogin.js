//sends the user input from login form to php
function authenticate() {

    //get user input
    var data = getLoginData();


    //ajax
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //callback function because it's asynchronous
            authorize(this.responseText);
        }
    }
    xmlhttp.open("POST", "php/login.php");
    xmlhttp.send(data);

}

//check if user has successfully authenticated by looking at php return value
function authorize(hasAccess) {
    //trim because spaces get added in the beginning and end (for whatever reason)
    if (hasAccess.trim() == "yes") {
        //hide modal and show content
        document.getElementById('login-modal').style.display='none';
        document.getElementById("allContent").classList.remove("hidden");
        initAdmin();
    } 
    else {
        unauthorized();
    }
}

//get user input from login modal
function getLoginData(){
    u = document.getElementById("u");
    p = document.getElementById("p");

    //needs to be formdata so it can be read in php $_POST variable
    var data = new FormData();
    data.append("user",u.value);
    data.append("pwd",p.value);

    return data;
}

//display error for bad credentials
function unauthorized(){
    var element = document.getElementById("tooBad");
    element.innerText = "Wrong credentials.";
    element.classList.remove("hidden");
}

//display cocky message for forgot-password
function tooBad() {
    var element = document.getElementById("tooBad");
    element.innerText = "Too bad. Ask someone of kegro, hivor, pandi, micmu and andwi. Credentials are pinned in a Slack conversation.";
    element.classList.remove("hidden");
}