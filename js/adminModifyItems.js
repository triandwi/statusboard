//open modal
function openUnknownModal(span) {
    //focus span to be able to target it later
    span.classList.add("focused");

    //show modal
    document.getElementById("unknownModal").style.display = "block";
}

//close modal
function closeUnknownModal(isSelected, li) {
    spanArray = document.getElementsByClassName("focused"); 
    span = spanArray[0];

    //replace "unknown" with person and make span unchangeable
    if (isSelected == "true") {
        span.innerText = li.innerText;

        span.classList.remove("changeable");
        span.removeAttribute("onclick");
    }

    //unfocus span
    span.classList.remove("focused");


    //hide modal
    document.getElementById("unknownModal").style.display = "none";
}

//create the list of buddies that is displayed when opening the modal
function createUlForModal(data) {

    //list of agents
    var list = document.getElementById("unknownChange");
    list.innerHTML = ""; //required for reload

    //one div per agent
    data.forEach(element => {
        var item = document.createElement("li");
        item.appendChild(document.createTextNode(element.ID));
        list.appendChild(item);
        item.setAttribute("onclick", "closeUnknownModal('true',this)");
        item.classList.add("changeable");

    });
}