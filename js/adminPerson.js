//delete or add person
function managePerson(mode) {
    var status;

    //get selected value from dropdown
    if (mode == "delete") {
        dropdown = document.getElementById("remove");
        person = dropdown.options[dropdown.selectedIndex].value;
    }
    //get value of input
    else {

        textbox = document.getElementById("addAgent");
        person = textbox.value;
    }

    var xhp = new XMLHttpRequest();
    xhp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //callback function because it's asynchronous
            status = this.responseText.trim();
            personValidate(status);

        }
    }
    xhp.open("GET", "php/managePerson.php?p=" + person + "&m=" + mode);
    xhp.send();
}

//generate person list
function displayPerson(data) {

    //list of agents
    var list = document.getElementById("person");
    list.innerHTML = ""; //required for reload

    //one div per agent
    data.forEach(element => {
        var item = document.createElement("li");
        item.appendChild(document.createTextNode(element.ID));
        list.appendChild(item);

    });

    //create the dropdown needed for remove

    //set errorbox empty if filled
    validateError("off");

}

//create the dropdown with the list of persons for person management
function createDropdownPerson(data) {
    //get the parent
    var parent = document.getElementById("removeAgent");
    //set it to zero (needed for reload)
    parent.innerHTML = "";

    //create and set style
    var dropdown = document.createElement("select");
    dropdown.setAttribute("id", "remove");
    dropdown.classList.add("w3-select");

    //create options
    data.forEach(element => {
        var option = document.createElement("option")
        option.appendChild(document.createTextNode(element.ID));
        option.setAttribute("value", element.ID);
        dropdown.appendChild(option);

    });

    //append to parent
    parent.appendChild(dropdown);

}

//check if person is valid
function personValidate(status) {
    //reload all person dropdowns if status is fine
    if (status == "good") {
        load('person', displayPerson);
        load('person', createDropdownItem);
        load('person', createDropdownPerson);
        load('person', createDropdownItemBuddy);
    }
    //error for wrong input
    else {
        validateError("on");

    }
}

//person is not valid
function validateError(toggle) {
    //get elements
    var textbox = document.getElementById("addAgent");
    var error = "Must be the lowercase 5 letter ID."
    var errorbox = document.getElementById("errorbox");

    //set style
    if (toggle == "on") {
        textbox.classList.add("w3-border");
        textbox.classList.add("w3-border-red");
        errorbox.classList.add("w3-padding");
        errorbox.classList.add("w3-red");
        errorbox.innerHTML = error;
    }
    //unset style
    else {
        textbox.classList.remove("w3-border");
        textbox.classList.remove("w3-border-red");
        errorbox.classList.remove("w3-padding");
        errorbox.classList.remove("w3-red");
        errorbox.innerHTML = "";
    }
}