//to be called when login is successfull. loads all things that require DB query for the data
function initAdmin() {
    //item management
    load('oooItem', displayItem); //item list
    load('person', createDropdownItem); //dropdown for person for the item
    load('person', createDropdownItemBuddy); //dropdown for buddy when sick
    load('person', createDropdownItemVacation); //dropdown for buddy when vacation
    load('person', createUlForModal); //dropdown fo rmodal when changing unknown
    setMinDateFrom();

    //person management
    load('person', displayPerson); //list
    load('person', createDropdownPerson); //dropdown for deletion

}