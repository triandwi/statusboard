//display the modal upon page load (modal is hidden by default)
window.onload = function initLogin() {
    document.getElementById('login-modal').style.display = 'block';

}

//to avoid reloading of the form upon submission, override the submit event and stop reloading with preventDefault
document.getElementById("loginForm").addEventListener("submit", function (e) {
    authenticate();
    e.preventDefault();
});
