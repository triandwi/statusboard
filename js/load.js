function load(param, callback) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //callback function because it's asynchron
            //parse to make JSOn from text
            callback(JSON.parse(this.responseText));
        }
    }
    xmlhttp.open("GET", "php/getData.php?q=" + param);
    xmlhttp.send();
}
