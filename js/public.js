function displayPublic(data) {
    // create li for each json object
    data.forEach(element => {
        //get correct list
        var list = document.getElementById(element.type)

        // create structure: li = parent, one div for person, one div for info
        var item = document.createElement("li");

        var person = document.createElement("div");
        person.appendChild(document.createTextNode(element.person));
        person.classList.add("w3-xlarge");
        person.classList.add("bold");

        item.appendChild(person);
        list.appendChild(item);

        var info = document.createElement("div");

        // cases for buddy and empty
        switch (element.type) {
            case "other":
            case "home":
            case "triage":
            case "long":
                if (element.info != "") {
                    info.appendChild(document.createTextNode(element.info));

                }
                else {
                    info.appendChild(document.createTextNode("-"));
                }
                break;

            case "sick":
                info.appendChild(document.createTextNode("Buddy: " + element.info));
                break;
            case "vacation":
                var buddyDiv = document.createElement("div");
                var durationDiv = document.createElement("div");
                var split = element.info.split(" | ")

                buddyDiv.appendChild(document.createTextNode("Buddy: " + split[0]));
                durationDiv.appendChild(document.createTextNode(split[1]));

                info.appendChild(buddyDiv);
                info.appendChild(durationDiv);

                //check if vacation is upcoming or not
                dates = split[1].split(" - ");
                var startDate = new Date(dates[0]);
                var today = new Date(Date.now());

                if (today < startDate) {
                    //item.classList.add("upcoming");
                    item.classList.add("italic");
                    item.classList.add("w3-pale-red");
                    
                }
                else { item.classList.add("w3-pale-green");}
            }

        // create div for additinal info
        info.classList.add("w3-large");
        item.appendChild(info);

    });
    sortVacationPublic();

}

function sortVacationPublic() {
    var list = document.getElementById("vacation");
    var switching, shouldswitch, b, i;

    switching = true;

    //loop that will continue until no switching has been done
    while (switching) {
    
        switching = false;
        b = list.getElementsByTagName("LI");

        //loop through list
        for (i = 0; i < (b.length - 1); i++) {
            shouldswitch = false;


            //check if switching should happen
            if (b[i].classList.contains("italic") && !b[i+1].classList.contains("italic")) {
                shouldswitch = true;
                break;
            }
        }

        //do the switch
        if (shouldswitch) {
            b[i].parentNode.insertBefore(b[i+1],b[i]);
            switching = true;
        }
    }

}