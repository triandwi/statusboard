<?php
require 'dbConnect.php';

//get query parameter from URL
$q = $_REQUEST["q"];

//construct SQL query and fire
$sql = "SELECT * FROM bos." . $q . " ORDER BY ID ASC";
$result = $conn->query($sql);

// convert db result object to array
$resultArray = [];

while($row = $result->fetch_assoc()) {
    $resultArray[] = $row;
}

// send back as JSON
echo json_encode($resultArray);

//$sql->close;

require 'dbDisconnect.php'
?> 