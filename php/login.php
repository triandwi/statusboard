<?php
require 'dbConnect.php';

//unauthorized by default
$hasAccess = "no";

$user = $_POST["user"];

//construct query and fire
$stmt = $conn->prepare("SELECT pwd FROM `bos`.`login` WHERE user=?");
$stmt->bind_param('s', $user);
$stmt->execute();
$stmt->bind_result($hashedPwd);
$stmt->fetch();

//verify user input against hashed pw from DB
if(password_verify($_POST["pwd"],$hashedPwd)) {
    $hasAccess = "yes";
}

echo $hasAccess;

$stmt->close();
require 'dbDisconnect.php';
?>