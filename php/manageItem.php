<?php
require 'dbConnect.php';

//get posted data
$data = json_decode($_POST["data"]);


//truncate table
$truncate = "TRUNCATE `bos`.`oooItem`";
$truncateResult = $conn->query($truncate);


//INSERT data into DB
foreach ($data as $value) {
    $sql = $conn->prepare("INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES (?,?,?)");
    $sql->bind_param('sss', $value->person, $value->type, $value->info);
    $sql->execute();
    $sql->close;
}

require 'dbDisconnect.php';
?>