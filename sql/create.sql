CREATE SCHEMA `bos` ;

CREATE TABLE `bos`.`person` (
  `ID` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`ID`));

INSERT INTO `bos`.`person` (`id`) VALUES ('sasmi');
INSERT INTO `bos`.`person` (`id`) VALUES ('stesc');
INSERT INTO `bos`.`person` (`id`) VALUES ('marso');
INSERT INTO `bos`.`person` (`id`) VALUES ('hargr');
INSERT INTO `bos`.`person` (`id`) VALUES ('daned');
INSERT INTO `bos`.`person` (`id`) VALUES ('mansi');
INSERT INTO `bos`.`person` (`id`) VALUES ('domdu');
INSERT INTO `bos`.`person` (`id`) VALUES ('shuli');
INSERT INTO `bos`.`person` (`id`) VALUES ('davkl');
INSERT INTO `bos`.`person` (`id`) VALUES ('dahau');
INSERT INTO `bos`.`person` (`id`) VALUES ('felkr');
INSERT INTO `bos`.`person` (`id`) VALUES ('kegro');
INSERT INTO `bos`.`person` (`id`) VALUES ('hivor');
INSERT INTO `bos`.`person` (`id`) VALUES ('danmi');
INSERT INTO `bos`.`person` (`id`) VALUES ('andwi');
INSERT INTO `bos`.`person` (`id`) VALUES ('micmu');
INSERT INTO `bos`.`person` (`id`) VALUES ('ulrro');
INSERT INTO `bos`.`person` (`id`) VALUES ('danan');
INSERT INTO `bos`.`person` (`id`) VALUES ('gerka');
INSERT INTO `bos`.`person` (`id`) VALUES ('berha');
INSERT INTO `bos`.`person` (`id`) VALUES ('marto');
INSERT INTO `bos`.`person` (`id`) VALUES ('stegl');
INSERT INTO `bos`.`person` (`id`) VALUES ('canoe');
INSERT INTO `bos`.`person` (`id`) VALUES ('karwl');
INSERT INTO `bos`.`person` (`id`) VALUES ('albra');
INSERT INTO `bos`.`person` (`id`) VALUES ('marja');
INSERT INTO `bos`.`person` (`id`) VALUES ('pandi');

CREATE TABLE `bos`.`oooItem` (
  `person` VARCHAR(50) NOT NULL,
  `type` VARCHAR(50) NOT NULL,
  `info` VARCHAR(255) NULL,
  `ID` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID`));


INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('marso', 'home', '');
INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('danmi', 'sick', 'ulrro');
INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('pandi', 'other', 'workshop');
INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('danmi', 'triage', 'late');
INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('shuli', 'triage', 'core');
INSERT INTO `bos`.`oooItem` (`person`, `type`, `info`) VALUES ('gerka', 'triage', 'early');


CREATE TABLE `bos`.`login` (
  `user` NVARCHAR(50) NOT NULL,
  `pwd` NVARCHAR(255) NOT NULL,
  PRIMARY KEY (`user`));

INSERT INTO `bos`.`login` (`user`, `pwd`) VALUES ('Admin', '$2y$10$E3UouL6rkKclcYhqY.QGiOpCzh8c.u3Cb8sOYZBF9TXQkCQWBQSsO');
